const table = document.createElement('table');
const td = document.createElement('td');

table.className = 'table';
td.className = 'td';

function addTable() {
    document.body.appendChild(table);
    for (let x = 1; x <= 30; x++) {
        const tr = document.createElement('tr');
        tr.className = 'tr';
        for (let i = 1; i <= 30; i++) {
            let newTd = td.cloneNode(true);
            tr.appendChild(newTd);
        }
        let newTr = tr.cloneNode(true);
        table.appendChild(newTr);
    }
}

addTable();

document.body.addEventListener('click', (event) => {
    let target = event.target;
    if (!target.classList.contains('td')) {
        table.classList.toggle('invers')

    }
    else {
        target.classList.toggle('change');
    }
});


